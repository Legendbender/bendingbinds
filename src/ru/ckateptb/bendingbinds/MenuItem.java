package ru.ckateptb.bendingbinds;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

public abstract class MenuItem {

    protected List<String> lore = new ArrayList<String>();
    protected MenuBase menu;
    protected int number;
    protected MaterialData icon;
    protected String text;
    protected boolean isEnchanted = false;

    public MenuItem(String text, MaterialData icon, int number) {
        this.text = text;
        this.icon = icon;
        this.number = number;
    }

    public MenuItem(String string, MaterialData icon) {
        this(string, icon, 1);
    }

    public void setEnchanted(boolean bool) {
        this.isEnchanted = bool;
    }

    public MenuBase getMenu() {
        return menu;
    }

    public void setMenu(MenuBase menu) {
        this.menu = menu;
    }

    public int getNumber() {
        return number;
    }

    public MaterialData getIcon() {
        return icon;
    }

    public String getText() {
        return text;
    }

    @SuppressWarnings("deprecation")
    public ItemStack getItemStack() {
        ItemStack slot = new ItemStack(getIcon().getItemType(), getNumber(), getIcon().getData());
        ItemMeta meta = slot.getItemMeta();
        meta.setLore(lore);
        meta.setDisplayName(getText());
        slot.setItemMeta(meta);
        return slot;//CraftItemStack.asCraftMirror(stack1);
    }

    /***DO NOT USE PLAYER VARIABLE IF USING MenuBendingOptions! Use the class' player variable instead! This causes
     * problems when looking at other player's menus. Called when a player clicks on the item.
     * @param player The player clicking*/
    public abstract void onClick(Player player);

    public void setDescriptions(List<String> lines) {
        this.lore = lines;
    }

    public void addDescription(String line) {
        this.lore.add(line);
    }

}