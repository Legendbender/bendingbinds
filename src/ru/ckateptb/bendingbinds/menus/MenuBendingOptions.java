package ru.ckateptb.bendingbinds.menus;

import com.projectkorra.projectkorra.BendingPlayer;
import com.projectkorra.projectkorra.Element;
import com.projectkorra.projectkorra.Element.SubElement;
import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.ComboAbility;
import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.ability.util.ComboManager;
import com.projectkorra.projectkorra.ability.util.MultiAbilityManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import ru.ckateptb.bendingbinds.*;

import java.util.*;

public class MenuBendingOptions extends MenuBase {
    protected Mode mode = Mode.NONE;
    protected boolean redirect = false;
    protected OfflinePlayer thePlayer;
    protected Player openPlayer = null;
    int movePage = 0;

    private List<String> playerMoves = new ArrayList<String>();
    private List<String> playerCombos = new ArrayList<String>();
    private String abilityIndex = null; //What ability is currently selected
    private int abilityIndexInt = -1;      //Used only for glow
    private int slotIndex = -1;


    public MenuBendingOptions(OfflinePlayer player) {
        super("Available abilities", 2);
        this.thePlayer = player;
      
		BendingPlayer bPlayer = BendingPlayer.getBendingPlayer(player);
		if (bPlayer != null && bPlayer.getElements().isEmpty())
		{
			redirect = true;
		}	
    }

    @SuppressWarnings("deprecation")
    public MenuItem getItemForMove(final String move, final int index) {
        MaterialData mat = new MaterialData(Material.STAINED_GLASS, (byte) 4);
        Element mainElement = CoreAbility.getAbility(move).getElement();
        if (mainElement == Element.AIR) {
            mat = Config.elementIcons.get(Element.AIR);
        } else if (mainElement == Element.EARTH) {
            mat = Config.elementIcons.get(Element.EARTH);
        } else if (mainElement == Element.FIRE) {
            mat = Config.elementIcons.get(Element.FIRE);
        } else if (mainElement == Element.WATER) {
            mat = Config.elementIcons.get(Element.WATER);
        } else if (mainElement == Element.CHI) {
            mat = Config.elementIcons.get(Element.CHI);
        } else {
            mat = Config.avatarIcon;
        }

        if (mainElement == Element.BLOOD) mat = Config.elementIcons.get(Element.BLOOD);
        else if (mainElement == Element.ICE) mat = Config.elementIcons.get(Element.ICE);
        else if (mainElement == Element.LAVA) mat = Config.elementIcons.get(Element.LAVA);
        else if (mainElement == Element.SAND) mat = Config.elementIcons.get(Element.SAND);
        else if (mainElement == Element.PLANT) mat = Config.elementIcons.get(Element.PLANT);
        else if (mainElement == Element.METAL) mat = Config.elementIcons.get(Element.METAL);
        else if (mainElement == Element.COMBUSTION) mat = Config.elementIcons.get(Element.COMBUSTION);
        else if (mainElement == Element.FLIGHT) mat = Config.elementIcons.get(Element.FLIGHT);
        else if (mainElement == Element.HEALING) mat = Config.elementIcons.get(Element.HEALING);
        else if (mainElement == Element.LIGHTNING) mat = Config.elementIcons.get(Element.LIGHTNING);
        else if (mainElement == Element.SPIRITUAL) mat = Config.elementIcons.get(Element.SPIRITUAL);

        if (mainElement instanceof SubElement) {
            mainElement = ((SubElement) mainElement).getParentElement();
        }

        final ChatColor c = mainElement == Element.AIR ? ChatColor.GRAY : (mainElement == Element.CHI ? ChatColor.DARK_PURPLE : (mainElement == Element.EARTH ? ChatColor.GREEN : (mainElement == Element.FIRE ? ChatColor.RED : (mainElement == Element.WATER ? ChatColor.BLUE : (mainElement == Element.AVATAR ? ChatColor.GOLD : mainElement.getColor())))));

        MenuItem item = new MenuItem(c + move.toString(), mat) {

            @Override
            public void onClick(Player player) {
                if (!GeneralMethods.abilityExists(move)) {
                    player.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "Move doesn't exist! Please contact Ozgun_ about this!");
                    BendingBinds.log.warning("[BendingGui] Error: Move selected with invalid ID/Name. Move name: \"" + move + "\". Please contact Ozgun_ about this!");
                    closeMenu(player);
                    return;
                }

                if (mode == Mode.INFO && move != null) {
                    player.sendMessage(c + CoreAbility.getAbility(move).getDescription());
                    closeMenu(player);
                    return;
                }
                if (mode == Mode.DELETE) {
                    mode = Mode.NONE;
                }

                if (slotIndex == -1) {
                    abilityIndex = move != abilityIndex ? move : null;
                    abilityIndexInt = index != abilityIndexInt ? index : -1;
                } else {
                    bindMoveToSlot(player, move, slotIndex);
                    slotIndex = -1;
                    abilityIndex = null;
                    abilityIndexInt = -1;
                }
                update();
                DynamicUpdater.updateMenu(thePlayer, getInstance());
            }
        };
        String desc = "";
        String moveDesc = GRAY + Descriptions.getDescription(move);
        List<String> l = BendingBinds.getDescriptions(moveDesc, GRAY, 45);
        for (String s : l) {
            desc = desc + s + "\n";
        }
        if (move != null && this.mode == Mode.DELETE) {
            desc = desc + "\n\n" + ChatColor.RED + ChatColor.BOLD + "TOGGLE THE REMOVAL TOOL BEFORE\n" + ChatColor.RED + ChatColor.BOLD + "REBINDING!" + "\n" + ChatColor.RESET + GRAY + "You must turn off the unbind tool before you\n" + GRAY + "can rebind moves again!";
        } else if (move != null && this.mode == Mode.INFO) {
            desc = desc + "\n\n" + ChatColor.YELLOW + ChatColor.BOLD + "CLICK FOR MORE INFO!" + "\n" + ChatColor.RESET + GRAY + "Click to display more infomation about this move!";
        } else if (this.abilityIndex == move) {
            desc = desc + "\n\n" + ChatColor.GREEN + ChatColor.BOLD + "SELECTED!" + "\n" + ChatColor.RESET + GRAY + "Click on the slot to bind this ability to it.!";
        }
        item.setDescriptions(Arrays.asList(desc.split("\n")));
        if (abilityIndex == move) {
            item.setEnchanted(true);
        }

        return item;
    }

    @SuppressWarnings("deprecation")
    public MenuItem getItemForSlot(OfflinePlayer player, final int index) {
        MaterialData mat = new MaterialData(Material.STAINED_GLASS_PANE, (byte) 15);
        final String move = this.getMoveForSlot(player, index + 1);
        ChatColor c = ChatColor.RED;
        if (move != null && !move.equals("null")) {
            Element element = CoreAbility.getAbility(move).getElement();
            if (element instanceof SubElement) element = ((SubElement) element).getParentElement();
            c = element == Element.AIR ? ChatColor.GRAY : (element == Element.CHI ? ChatColor.DARK_PURPLE : (element == Element.EARTH ? ChatColor.GREEN : (element == Element.FIRE ? ChatColor.RED : (element == Element.WATER ? ChatColor.BLUE : (element == Element.AVATAR ? ChatColor.GOLD : element.getColor())))));
            if (element == Element.AIR) {
                mat.setData((byte) 0);
            } else if (element == Element.EARTH) {
                mat.setData((byte) 5);
            } else if (element == Element.FIRE) {
                mat.setData((byte) 14);
            } else if (element == Element.WATER) {
                mat.setData((byte) 11);
            } else if (element == Element.CHI) {
                mat.setData((byte) 10);
            } else {
                mat.setData((byte) 4);
            }
        }
        final ChatColor c1 = c;


        String itemname, desc = "";

        if (move == null || move.equals("null")) {
            itemname = ChatColor.RED + "Slot " + (index + 1) + GRAY + " (Empty)";
            desc = GRAY + "Need to bind the ability to the slot!\n\n"
                    + GRAY + "Click on the ability, then onto the slot!";
        } else {
            itemname = c + "Slot " + (index + 1) + GRAY + " (" + move.toString() + ")";
            desc = GRAY + "The ability is bound to the slot: " + c + ChatColor.BOLD + move.toString() + "\n\n";
        }

        if (MultiAbilityManager.playerAbilities.containsKey((Player) player)) {
            desc = desc + "\n\n" + ChatColor.RED + ChatColor.BOLD + "YOU CANNOT EDIT YOUR BINDS RIGHT NOW!\n" + ChatColor.RESET + GRAY + "You are using a multi-ability move and must stop\nusing it before you can bind again!";
        } else if (thePlayer instanceof Player) {
            if (move != null && mode == Mode.DELETE) {
                desc = desc + "\n\n" + ChatColor.RED + ChatColor.BOLD + "CLICK TO REMOVE!" + "\n" + ChatColor.RESET + GRAY + "Click to remove " + move.toString() + " from this slot!";
            } else if (move != null && mode == Mode.INFO) {
                desc = desc + "\n\n" + ChatColor.YELLOW + ChatColor.BOLD + "CLICK FOR MOVE INFO!" + "\n" + ChatColor.RESET + GRAY + "Click to display more infomation about " + move.toString() + "!";
            } else if (this.slotIndex == index) {
                desc = desc + "\n\n" + ChatColor.GREEN + ChatColor.BOLD + "NOW SELECTED!" + "\n" + ChatColor.RESET + GRAY + "Click on the ability to bind it to this ability!";
            }
        } else {
            desc = desc + "\n\n" + ChatColor.RED + ChatColor.BOLD + "CANNOT MODIFY BENDING OF OFFLINE PLAYERS!" + "\n" + ChatColor.RESET + GRAY + "You can't modify the bending of players that are offline!";
        }

        MenuItem item = new MenuItem(itemname, mat) {

            @Override
            public void onClick(Player player) {
                if (MultiAbilityManager.playerAbilities.containsKey(player)) {
                    closeMenu(player);
                    player.sendMessage(ChatColor.RED + "You cannot modify your binds right now!");
                } else if (thePlayer instanceof Player) {
                    if (mode == Mode.DELETE && move != null) {
                        BendingPlayer bPlayer = BendingPlayer.getBendingPlayer(thePlayer.getName());
                        HashMap<Integer, String> abilities = bPlayer.getAbilities();
                        abilities.remove(index + 1);
                        bPlayer.setAbilities(abilities);
                        GeneralMethods.saveAbility(bPlayer, index + 1, null);
                        player.sendMessage(ChatColor.RED + "Removed " + move.toString() + " from Slot " + (index + 1));
                        update();
                        return;
                    } else if (mode == Mode.INFO && move != null) {
                        player.sendMessage(c1 + CoreAbility.getAbility(move).getDescription());
                        closeMenu(player);
                        return;
                    }

                    if (abilityIndex == null) {
                        slotIndex = index != slotIndex ? index : -1;
                    } else {
                        bindMoveToSlot((Player) thePlayer, abilityIndex, index);
                        abilityIndex = null;
                        slotIndex = -1;
                        abilityIndexInt = -1;
                    }
                    update();
                    DynamicUpdater.updateMenu(thePlayer, getInstance());
                }
            }
        };
        item.setDescriptions(Arrays.asList(desc.split("\n")));
        if (index == this.slotIndex) {
            item.setEnchanted(true);
        }
        return item;
    }

    /**
     * Shows lots of info for moves when you click them
     */
    public MenuItem getInfoToolItem(OfflinePlayer player) {
        MaterialData material = new MaterialData(Material.SIGN);
        String s = "Move Help Tool " + GRAY + (this.mode == Mode.INFO ? "(On)" : "(Off)");
        s = ChatColor.YELLOW + s;
        MenuItem item = new MenuItem(s, material) {

            @Override
            public void onClick(Player player) {
                mode = mode == Mode.INFO ? Mode.NONE : Mode.INFO;
                update();
                DynamicUpdater.updateMenu(thePlayer, getInstance());
            }
        };
        item.setDescriptions(Arrays.asList(new String[]{ChatColor.GRAY + "When toggled on, click on an ability for more information.",
                ChatColor.GRAY + "Click to turn " + (this.mode == Mode.INFO ? "off again" : "on")}));
        if (this.mode == Mode.INFO) {
            item.setEnchanted(true);
        }
        return item;
    }

    /**
     * The arrow for moving pages
     */
    public MenuItem getPageArrow(OfflinePlayer player, final boolean isRightDirection) {
        MaterialData material = new MaterialData(Material.ARROW);
        String s = ChatColor.YELLOW + (isRightDirection ? "Next page" : "Previous page ");
        s = s + GRAY + "(" + ChatColor.YELLOW + (this.movePage + 1) + GRAY + "/" + ChatColor.YELLOW + this.getMaxPages() + GRAY + ")";
        MenuItem item = new MenuItem(s, material) {


            @Override
            public void onClick(Player player) {
                //Import not to use the player variable from this method. If you do, the menu will change
                //to that players which stops functionality for admins looking at other player's bindings
                if (movePage == getMaxPages() - 1 && isRightDirection) return;
                movePage = isRightDirection ? movePage + 1 : movePage - 1;
                DynamicUpdater.setPage(thePlayer, movePage);

                if (BendingBinds.pageArrowMoveMouse) {
                    MenuBendingOptions menu = new MenuBendingOptions(thePlayer);
                    menu.movePage = movePage;
                    getMenu().getInventory().clear();
                    closeMenu(player);
                    menu.openMenu(player);
                    //Has to use normal player here because we want it to show to the player who's using the menu
                } else {
                    update();
                }
                DynamicUpdater.updateMenu(thePlayer, getInstance());
            }
        };
        item.setDescriptions(Arrays.asList(new String[]{ChatColor.GRAY + "Click to go to " + (isRightDirection ? "next" : "previous") + " ability page."}));
        return item;
    }

    /**
     * Returns the move based on the slot
     */
    public String getMoveForSlot(OfflinePlayer player, int index) {
        try {
            return BendingPlayer.getBendingPlayer(player).getAbilities().get(index);
        } catch (NullPointerException e) {
            return null;
        }
    }

    /**
     * Bind the move to the slot. Args: Move, slot. Slot should be an int from 0 to 8
     */
    public void bindMoveToSlot(Player player, String move, int slot) {
        if (slot >= 9) {
            openPlayer.sendMessage(ChatColor.RED + "Error: Slot binding out of range! Please contact your admin about this!");
            this.closeMenu(openPlayer);
            return;
        }

        BendingPlayer.getBendingPlayer(player.getName()).getAbilities().put(slot + 1, move);
        GeneralMethods.saveAbility(BendingPlayer.getBendingPlayer(player.getName()), slot + 1, move);
        Element e = CoreAbility.getAbility(move).getElement();
        if (e instanceof SubElement) e = ((SubElement) e).getParentElement();
        ChatColor c = e == Element.AIR ? ChatColor.GRAY : (e == Element.CHI ? ChatColor.DARK_PURPLE : (e == Element.EARTH ? ChatColor.GREEN : (e == Element.FIRE ? ChatColor.RED : (e == Element.WATER ? ChatColor.BLUE : ChatColor.GOLD))));
        player.sendMessage(c + move + ChatColor.YELLOW + " has been bound to the slot " + (slot + 1) + "!");
    }

    /**
     * Updates the GUI by re-initializing
     */
    public void update() {
        OfflinePlayer player = this.thePlayer;

        for (int i = 0; i < this.getInventory().getContents().length; i++) {
            if (this.getInventory().getContents()[i] == null) {
                this.getInventory().clear(i);
            } else if (this.getInventory().getContents()[i].getType() != Material.SKULL_ITEM) {
                this.getInventory().clear(i);
            }
        }
        this.playerMoves.clear();
        this.playerCombos.clear();

        HashMap<Element, List<CoreAbility>> abilities = new HashMap<Element, List<CoreAbility>>();

        mainloop:
        for (CoreAbility ability : CoreAbility.getAbilities()) {
            if ((player instanceof Player && BendingPlayer.getBendingPlayer(player).canBind(ability)) || ((!(player instanceof Player)) && !this.playerMoves.contains(ability.getName()))) {
                if (ability.isHiddenAbility()) continue;
                if (ComboManager.getComboAbilities().containsKey(ability.getName()) || ability instanceof ComboAbility)
                    continue;
                if (!abilities.containsKey(ability.getElement())) {
                    abilities.put(ability.getElement(), new ArrayList<CoreAbility>());
                }

                for (CoreAbility ability2 : abilities.get(ability.getElement())) {
                    if (ability2.getName().equals(ability.getName())) {
                        continue mainloop;
                    }
                }
                abilities.get(ability.getElement()).add(ability);
            }
        }

        for (Element element : BendingBinds.elementOrder) {
            List<CoreAbility> abilities_ = abilities.get(element);
            if (abilities_ == null || abilities_.isEmpty()) continue;
            List<String> abilitylist = new ArrayList<String>();
            for (CoreAbility ab : abilities_) {
                abilitylist.add(ab.getName());
            }

            Collections.sort(abilitylist);
            this.playerMoves.addAll(abilitylist);


        }

        //Hopefully fix bug
        if (this.playerMoves.isEmpty()) {
            BukkitRunnable run = new BukkitRunnable() {

                public void run() {
                    update();
                }
            };
            run.runTaskLater(BendingBinds.INSTANCE, 200L);
        }


        int maxPage = this.getMaxPages();

        int p = DynamicUpdater.getPage(thePlayer);
        if (p > this.getMaxPages()) {
            p = this.getMaxPages() - 1;
        }
        movePage = p;

        //The first index for the move to get from the list
        int firstMoveIndex = (movePage == 0 ? 0 : movePage * 7 + 1);
        int firstIndex_ = movePage == 0 ? 0 : 1;

        int end; //Last index to use

        int size = this.playerMoves.size();

        // Find the end of the relevant part of the list.
        if (movePage >= maxPage - 1) end = size;
        else if (movePage == 0) end = 8;
        else end = firstMoveIndex + 7;

        if (end >= size) end = size;

        for (int i = firstMoveIndex; i < end; ++i) {
            int slotIndex = movePage == 0 ? i : (i - firstMoveIndex + firstIndex_);

            MenuItem item;
            String move = this.playerMoves.get(i);
            item = this.getItemForMove(move, slotIndex);

            this.addMenuItem(item, slotIndex);
        }

        //Add arrows
        if (this.movePage != 0) {
            this.addMenuItem(this.getPageArrow(player, false), 0);
        }
        if (this.movePage != maxPage - 1) {
            this.addMenuItem(this.getPageArrow(player, true), 8);
        }

        //Add the user slot items
        for (int i = 0; i < 9; i++) {
            MenuItem item = this.getItemForSlot(player, i);
            this.addMenuItem(item, i, 1);

        }
    }

    @Override
    public void openMenu(Player player) {
    
        this.openPlayer = player;
        this.playerMoves = new ArrayList<String>();
        this.playerCombos = new ArrayList<String>();

if (this.redirect)
		{
			if (!player.hasPermission("bending.command.choose")) 
			{
				player.sendMessage(ChatColor.RED + "You must have an element to modify your bending!");
				return;
			}
			player.sendMessage(ChatColor.GREEN + "You aren't a bender yet! Please choose an element!");
			this.switchMenu(player, new MenuSelectElement(thePlayer));
			redirect = false;
			return;
		}

        int p = DynamicUpdater.getPage(thePlayer);
        if (p > this.getMaxPages()) {
            p = this.getMaxPages();
        }
        this.movePage = p;

        update();
        super.openMenu(player);
    }

    @Override
    public void closeMenu(Player player) {
        super.closeMenu(player);
    }

    public int getMaxPages() {
        int i = this.playerMoves.size();
        if (i <= 9) return 1;
        if (i <= 16) return 2;
        return ((i - 3) / 7) + 1;
    }

    public MenuBendingOptions getInstance() {
        return this;
    }

    public OfflinePlayer getOpenPlayer() {
        return openPlayer;
    }

    public OfflinePlayer getMenuPlayer() {
        return thePlayer;
    }

    /**
     * Transfers important information over from the argument menu to this current menu
     */
    public void updateFromMenu(MenuBendingOptions menu) {
        this.abilityIndex = menu.abilityIndex;
        this.abilityIndexInt = menu.abilityIndexInt;
        this.mode = menu.mode;
        this.movePage = menu.movePage;
        this.slotIndex = menu.slotIndex;
    }

    public enum Mode {
        NONE, DELETE, INFO
    }
}